export default function UserCard({data}) {

    return (<div className="team-card">
        <img src={data?.img} alt={data?.first_name}></img>
        <div style={{ display: 'flex', flexDirection: 'column', textAlign: 'left', justifyContent: 'center' }}>
            <div className="card-title">
                {data?.first_name} {data?.last_name}
            </div>
            <div className="card-description">{data?.email}</div>
        </div>
    </div>)
}