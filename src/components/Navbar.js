import React from "react";

export default function Navbar({handleSearch}) {

    return (
        <nav className="navbar">
            <div className="navbar-container">
                <a href="/">Team</a>
                <div className="nav-search-container">
                    <img src="/search.png" alt="search-icon" />
                    <input className="search-input" type="text" placeholder="Search" onChange={handleSearch} />
                </div>
            </div>
        </nav>
    )

}