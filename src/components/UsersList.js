import { useMemo } from "react"
import UserCard from "./UserCard"

export default function UsersList({ title, users }) {

  const usersList = useMemo(() => {
    return users.map((data, index) => {
      return <div key={data.email} className="grid-item">
        <UserCard data={data} />
      </div>
    })
  }, [users])

  return <section>
    <h2 className='section-title'>{title}</h2>
    <div className='container-grid'>
      {usersList}
    </div>
  </section>
}