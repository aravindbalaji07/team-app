export default async function getUsers() {
    try {
        const response = await fetch('https://mocki.io/v1/ddb7e0a8-e218-4e36-b1be-b902cdb1c098')
        const data = await response.json()
        return {success: true, data}
    } catch (error) {
        return {success: false, error}
    }
}