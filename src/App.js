import { useEffect, useMemo, useState } from 'react';
import './App.css';
import Navbar from './components/Navbar';
import getUsers from './api';
import UsersList from './components/UsersList';
import { debounce } from 'lodash';

function App() {
  const [users, setUsers] = useState([])
  const [searchText, setSearchText] = useState(null)

  useEffect(() => {
    fetchUsers()
  }, [])

  const fetchUsers = async () => {
    const response = await getUsers()
    if (response?.data) {
      setUsers(response.data)
    }
  }

  const handleSearch = debounce((e) => {
    setSearchText(e.target.value)
  }, 300)

  const [administrators, members] = useMemo(() => {
    return users.reduce(([admins, mems], user) => {
      const isUserMatch = searchText ? user?.first_name?.toLowerCase().includes(searchText.toLowerCase()) : true
      if (user?.role === 'admin' && isUserMatch) {
        admins.push(user)
      } else if (user?.role === 'member' && isUserMatch) {
        mems.push(user)
      }
      return [admins, mems]
    }, [[], []])

  }, [users, searchText])

  return (
    <div className="App">
      <Navbar handleSearch={handleSearch} />
      <main style={{marginTop: '100px'}}>
        <UsersList title="Administrators" users={administrators} />
        <div className='divider'></div>
        <UsersList title="Members" users={members} />
        <button className='float-button'>+</button>
      </main>
    </div>
  );
}

export default App;
